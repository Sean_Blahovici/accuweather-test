/*
 * Copyright (c) 2020, Sean Blahovici - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sean Blahovici <sean.blahovici@gmail.com>, 15/04/20 10:04 AM
 */

@file:Suppress("MayBeConstant")

object Versions {
	//Kotlin
	val kotlin = "1.3.72"
	val kotlinCoroutines = "1.3.2"

	//Google affiliated
	val gradleTools = "4.0.0"
	val glide = "4.10.0"
	val retrofit = "2.9.0"
	val junit = "4.13"

	//3rd party libs
	val adapterDelegates = "4.3.0"
	val kluent = "1.61"
	val nhaarmanMockitoKotlin = "2.2.0"
	val mockitoAndroid = "3.3.3"
	val arrow = "0.10.5"
	val koin = "2.0.1"

	//AndroidX
	val navigation = "2.3.0"
	val cardView = "1.0.0"
	val materialComponents = "1.2.0-beta01"
	val appCompat = "1.2.0-rc01"
	val recyclerView = "1.2.0-alpha04"
	val androidAnnotations = "1.1.0"
	val constraintLayout = "2.0.0-beta7"

	//AndroidX Testing
	val testCore = "1.3.0-rc01"
	val idling = "3.3.0-rc01"
	val archCoreTesting = "2.1.0"
	val archComponents = "2.3.0-alpha05"
	val lifecycleExtensions = "2.2.0"
}

object Deps {
	//Kotlin
	val kotlin = "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlin}"
	val kotlinReflect = "org.jetbrains.kotlin:kotlin-reflect:${Versions.kotlin}"
	val kotlinGradle = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"
	val kotlinCoroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.kotlinCoroutines}"
	val kotlinCoroutinesTest = "org.jetbrains.kotlinx:kotlinx-coroutines-test:${Versions.kotlinCoroutines}"

	//Google affiliated
	val androidGradle = "com.android.tools.build:gradle:${Versions.gradleTools}"
	val glide = "com.github.bumptech.glide:glide:${Versions.glide}"
	val retrofitConverter = "com.squareup.retrofit2:converter-gson:${Versions.retrofit}"
	val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofit}"
	val junit = "junit:junit:${Versions.junit}"
	val adapterDelegates = "com.hannesdorfmann:adapterdelegates4:${Versions.adapterDelegates}"
	val kluent = "org.amshove.kluent:kluent:${Versions.kluent}"
	val mockito = "com.nhaarman.mockitokotlin2:mockito-kotlin:${Versions.nhaarmanMockitoKotlin}"
	val mockitoAndroid = "org.mockito:mockito-android:${Versions.mockitoAndroid}"
	val koin = "org.koin:koin-androidx-viewmodel:${Versions.koin}"

	//Arrow
	val arrowCore = "io.arrow-kt:arrow-core:${Versions.arrow}"
	val arrowSyntax = "io.arrow-kt:arrow-syntax:${Versions.arrow}"
	val arrowMeta = "io.arrow-kt:arrow-meta:${Versions.arrow}"

	//AndroidX
	val navigationFragment = "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}"
	val navigationUI = "androidx.navigation:navigation-ui-ktx:${Versions.navigation}"
	val constraintLayout = "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}"
	val androidAnnotations = "androidx.annotation:annotation:${Versions.androidAnnotations}"
	val lifeCycleExtensions = "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycleExtensions}"
	val archComponentsCompiler = "androidx.lifecycle:lifecycle-compiler:${Versions.archComponents}"
	val appCompat = "androidx.appcompat:appcompat:${Versions.appCompat}"
	val cardView = "androidx.cardview:cardview:${Versions.cardView}"
	val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerView}"
	val materialComponents = "com.google.android.material:material:${Versions.materialComponents}"
	val viewModelKTX = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.archComponents}"
	val liveDataKTX = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.archComponents}"

	//AndroidX Testing
	val testCore = "androidx.test:core:${Versions.testCore}"
	val archCoreTesting = "androidx.arch.core:core-testing:${Versions.archCoreTesting}"
	val idling = "androidx.test.espresso:espresso-idling-resource:${Versions.idling}"
}