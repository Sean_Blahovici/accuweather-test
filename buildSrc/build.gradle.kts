/*
 * Copyright (c) 2020, Sean Blahovici - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sean Blahovici <sean.blahovici@gmail.com>, 15/04/20 10:04 AM
 */

plugins {
    `kotlin-dsl`
}

repositories {
    jcenter()
}