package com.blahovici.accuweather_test.core.ui

data class FailureItemView(val overline: String,
                           val message: String)