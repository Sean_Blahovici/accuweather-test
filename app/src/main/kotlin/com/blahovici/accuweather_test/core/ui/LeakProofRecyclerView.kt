package com.blahovici.accuweather_test.core.ui

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView

class LeakProofRecyclerView constructor(context: Context, attrs: AttributeSet)  : RecyclerView(context, attrs) {
	override fun onDetachedFromWindow() {
		super.onDetachedFromWindow()
		if (adapter != null) {
			adapter = null
		}
	}
}