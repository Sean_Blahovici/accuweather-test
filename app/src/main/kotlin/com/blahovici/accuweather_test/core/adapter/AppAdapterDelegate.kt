package com.blahovici.accuweather_test.core.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hannesdorfmann.adapterdelegates4.AdapterDelegate

abstract class AppAdapterDelegate<T> : AdapterDelegate<T>() {
	protected lateinit var activityContext: Context

	override fun onCreateViewHolder(parent: ViewGroup): RecyclerView.ViewHolder {
		activityContext = parent.context
		return AppViewHolder(LayoutInflater.from(activityContext)
				.inflate(layoutId(), parent, false))
	}

	class AppViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

	abstract fun layoutId(): Int
}