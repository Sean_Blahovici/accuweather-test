package com.blahovici.accuweather_test.core.ui

import androidx.recyclerview.widget.RecyclerView
import com.blahovici.accuweather_test.R
import com.blahovici.accuweather_test.core.adapter.AppAdapterDelegate
import kotlinx.android.synthetic.main.row_title.view.*

class TitleAdapterDelegate : AppAdapterDelegate<List<Any>>() {
	override fun layoutId(): Int = R.layout.row_title

	override fun isForViewType(items: List<Any>, position: Int): Boolean =
			items[position] is TitleView

	override fun onBindViewHolder(items: List<Any>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
		val view = items[position] as TitleView
		holder.itemView.apply {
			rowTitleText.text = view.title
		}
	}
}