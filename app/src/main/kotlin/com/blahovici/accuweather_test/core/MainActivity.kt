package com.blahovici.accuweather_test.core

import android.content.Context
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.annotation.AttrRes
import androidx.appcompat.app.AppCompatActivity
import com.blahovici.accuweather_test.R
import kotlinx.android.synthetic.main.layout_main.*

class MainActivity : AppCompatActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.layout_main)
		setupStatusBar()
	}

	private fun setupStatusBar() {
		window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
		window.statusBarColor = colorIdFromAttribute(R.attr.colorPrimaryDark)
	}

	private fun colorIdFromAttribute(@AttrRes attrColor: Int, typedValue: TypedValue = TypedValue(), resolveRefs: Boolean = true): Int {
		theme.resolveAttribute(attrColor, typedValue, resolveRefs)
		return typedValue.data
	}

	fun hideKeyboard(v: View? = null) = (v ?: currentFocus)?.let { focusedView ->
		val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
		imm.hideSoftInputFromWindow(focusedView.windowToken, 0)
	}

	fun showProgress() {
		progressCircular.visibility = View.VISIBLE
	}

	fun hideProgress() {
		progressCircular.visibility = View.INVISIBLE
	}
}