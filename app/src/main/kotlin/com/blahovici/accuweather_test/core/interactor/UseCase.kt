package com.blahovici.accuweather_test.core.interactor

import arrow.core.Either
import com.blahovici.accuweather_test.core.failure.Failure
import kotlinx.coroutines.*

/**
 * By convention each [UseCase] implementation will execute its job in a background thread
 * (kotlin coroutine) and will post the result in the UI thread.
 */
abstract class UseCase<out Type, in Params> where Type : Any {
    abstract suspend fun run(params: Params): Either<Failure, Type>

    fun execute(onResult: (Either<Failure, Type>) -> Unit, params: Params, coroutineScope: CoroutineScope) {
        val job = coroutineScope.async(Dispatchers.IO) { run(params) }
        coroutineScope.launch(Dispatchers.Main) { onResult.invoke(job.await()) }
    }
}