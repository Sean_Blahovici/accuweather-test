package com.blahovici.accuweather_test.core.ui

data class TitleView(val title: String)