package com.blahovici.accuweather_test.core.di

import com.blahovici.accuweather_test.feature.news.NewsApi
import com.blahovici.accuweather_test.feature.news.NewsRepository
import com.blahovici.accuweather_test.feature.news.NewsResponseMapper
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val newsModule = module {
    single {
        Retrofit.Builder()
            .baseUrl(NewsApi.NEWS_BASE_URL)
            .client(OkHttpClient.Builder()
                .connectTimeout(NETWORK_CALL_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .readTimeout(NETWORK_CALL_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .addInterceptor { chain ->
                    chain.proceed(modifyRequestToRemoveFirstAmpersand(chain))
                }.build()
            )
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(NewsApi::class.java)
    }

    single<NewsRepository> { NewsRepository.Service(get(), get()) }
    factory<NewsResponseMapper> { NewsResponseMapper.Service() }
}

private fun modifyRequestToRemoveFirstAmpersand(chain: Interceptor.Chain): Request =
	chain.request().newBuilder()
		.url(chain.request().url().url().toString().replaceFirst("&", ""))
		.build()

private const val NETWORK_CALL_TIMEOUT_SECONDS = 30L