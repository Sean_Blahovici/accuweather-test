package com.blahovici.accuweather_test.core.ui

import androidx.recyclerview.widget.RecyclerView
import com.blahovici.accuweather_test.R
import com.blahovici.accuweather_test.core.adapter.AppAdapterDelegate
import kotlinx.android.synthetic.main.row_failure_item.view.*

class FailureItemAdapterDelegate : AppAdapterDelegate<List<Any>>() {
	override fun layoutId(): Int = R.layout.row_failure_item

	override fun isForViewType(items: List<Any>, position: Int): Boolean =
			items[position] is FailureItemView

	override fun onBindViewHolder(items: List<Any>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
		val view = items[position] as FailureItemView
		holder.itemView.apply {
			rowFailureItemTitle.text = view.overline
			rowFailureItemMessage.text = view.message
		}
	}
}