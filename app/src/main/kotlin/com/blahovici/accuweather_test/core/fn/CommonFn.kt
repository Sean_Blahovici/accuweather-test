package com.blahovici.accuweather_test.core.fn

fun <T> tryOrNull(fn: () -> T) : T? =
    try {
        fn.invoke()
    } catch (e: Throwable) {
        null
    }