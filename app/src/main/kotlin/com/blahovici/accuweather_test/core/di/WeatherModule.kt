package com.blahovici.accuweather_test.core.di

import com.blahovici.accuweather_test.feature.weather.WeatherApi
import com.blahovici.accuweather_test.feature.weather.WeatherRepository
import com.blahovici.accuweather_test.feature.weather.current.CurrentWeatherResponseMapper
import com.blahovici.accuweather_test.feature.weather.forecast.ForecastResponseMapper
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val weatherModule = module {
	single {
		Retrofit.Builder()
				.baseUrl(WeatherApi.OPEN_WEATHER_BASE_URL)
				.client(OkHttpClient.Builder()
						.connectTimeout(NETWORK_CALL_TIMEOUT_SECONDS, TimeUnit.SECONDS)
						.readTimeout(NETWORK_CALL_TIMEOUT_SECONDS, TimeUnit.SECONDS)
						.build())
				.addConverterFactory(GsonConverterFactory.create())
				.build().create(WeatherApi::class.java)
	}

	single<WeatherRepository> { WeatherRepository.Service(get(), get(), get()) }
	factory<CurrentWeatherResponseMapper> { CurrentWeatherResponseMapper.Service() }
	factory<ForecastResponseMapper> { ForecastResponseMapper.Service() }
}

private const val NETWORK_CALL_TIMEOUT_SECONDS = 30L