package com.blahovici.accuweather_test.core.failure

sealed class Failure {
    object NetworkConnection : Failure()

    abstract class FeatureFailure: Failure()
    abstract class ThirdPartyFailure(val apiResponse: Any?) : Failure()
}