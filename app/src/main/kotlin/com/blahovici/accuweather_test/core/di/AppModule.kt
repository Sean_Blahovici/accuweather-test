package com.blahovici.accuweather_test.core.di

import com.blahovici.accuweather_test.core.ui.FailureItemAdapterDelegate
import com.blahovici.accuweather_test.core.ui.TitleAdapterDelegate
import com.blahovici.accuweather_test.feature.city.BuildCityDataViews
import com.blahovici.accuweather_test.feature.city.CityDataAdapter
import com.blahovici.accuweather_test.feature.city.CityDataViewModel
import com.blahovici.accuweather_test.feature.city.FetchCityData
import com.blahovici.accuweather_test.feature.news.NewsHeadlineAdapterDelegate
import com.blahovici.accuweather_test.feature.weather.current.CurrentWeatherAdapterDelegate
import com.blahovici.accuweather_test.feature.weather.forecast.ThreeDayForecastAdapterDelegate
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
	factory { FetchCityData(get(), get()) }
	factory { BuildCityDataViews() }
	factory { CityDataAdapter(get(), get(), get(), get(), get()) }
	factory { CurrentWeatherAdapterDelegate() }
	factory { ThreeDayForecastAdapterDelegate() }
	factory { NewsHeadlineAdapterDelegate() }
	factory { TitleAdapterDelegate() }
	factory { FailureItemAdapterDelegate() }

	viewModel { CityDataViewModel(get(), get()) }
}