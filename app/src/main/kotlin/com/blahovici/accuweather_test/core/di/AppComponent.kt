package com.blahovici.accuweather_test.core.di

val appComponent = listOf(appModule, weatherModule, newsModule)