/*
 * Copyright (c) 2020, Sean Blahovici - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sean Blahovici <sean.blahovici@gmail.com>, 15/04/20 10:04 AM
 */

package com.blahovici.accuweather_test

import android.app.Application
import com.blahovici.accuweather_test.core.di.appComponent
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class AndroidApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidContext(this@AndroidApplication)
            modules(appComponent)
        }
    }
}