package com.blahovici.accuweather_test.feature.news

import androidx.recyclerview.widget.RecyclerView
import com.blahovici.accuweather_test.R
import com.blahovici.accuweather_test.core.adapter.AppAdapterDelegate
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_news_headline.view.*

class NewsHeadlineAdapterDelegate : AppAdapterDelegate<List<Any>>() {
	override fun layoutId(): Int = R.layout.row_news_headline

	override fun isForViewType(items: List<Any>, position: Int): Boolean = items[position] is NewsHeadlineView

	override fun onBindViewHolder(items: List<Any>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
		val view = items[position] as NewsHeadlineView
		holder.itemView.apply {
			rowNewsHeadlineTitle.text = view.title
			rowNewsHeadlineSource.text = view.bySourceLabel
			Glide.with(activityContext.applicationContext).load(view.imageUrl).into(rowNewsHeadlineImage)
		}
	}
}