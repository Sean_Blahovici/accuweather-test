package com.blahovici.accuweather_test.feature.weather

import arrow.core.Either
import com.blahovici.accuweather_test.core.failure.Failure
import com.blahovici.accuweather_test.core.fn.tryOrNull
import com.blahovici.accuweather_test.feature.weather.current.CurrentWeather
import com.blahovici.accuweather_test.feature.weather.current.CurrentWeatherResponseMapper
import com.blahovici.accuweather_test.feature.weather.forecast.ThreeDayForecast
import com.blahovici.accuweather_test.feature.weather.forecast.ForecastResponseMapper

interface WeatherRepository {
    fun fetchCurrent(cityName: String): Either<Failure, CurrentWeather>
    fun fetchForecast(cityName: String): Either<Failure, ThreeDayForecast>

    class Service(private val weatherApi: WeatherApi,
                  private val currentWeatherResponseMapper: CurrentWeatherResponseMapper,
                  private val forecastResponseMapper: ForecastResponseMapper) : WeatherRepository {
        override fun fetchCurrent(cityName: String): Either<Failure, CurrentWeather> {
            tryOrNull { weatherApi.currentWeather(cityName).execute() }?.let { apiResponse ->
                apiResponse.body()?.let { fetchedWeather ->
                    return Either.Right(currentWeatherResponseMapper.map(fetchedWeather))
                }
                return Either.Left(WeatherFailure.FetchCurrentWeatherError(apiResponse.toString()))
            } ?: return Either.Left(Failure.NetworkConnection)
        }

        override fun fetchForecast(cityName: String): Either<Failure, ThreeDayForecast> {
            tryOrNull { weatherApi.weatherForecast(cityName).execute() }?.let { apiResponse ->
                apiResponse.body()?.let { fetchedForecast ->
                    return Either.Right(forecastResponseMapper.map(fetchedForecast))
                }
                return Either.Left(WeatherFailure.FetchForecastError(apiResponse.toString()))
            } ?: return Either.Left(Failure.NetworkConnection)
        }
    }
}