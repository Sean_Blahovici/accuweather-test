package com.blahovici.accuweather_test.feature.city

import com.blahovici.accuweather_test.core.adapter.AutoUpdatableAdapter
import com.blahovici.accuweather_test.core.ui.FailureItemAdapterDelegate
import com.blahovici.accuweather_test.core.ui.TitleAdapterDelegate
import com.blahovici.accuweather_test.feature.news.NewsHeadlineAdapterDelegate
import com.blahovici.accuweather_test.feature.weather.current.CurrentWeatherAdapterDelegate
import com.blahovici.accuweather_test.feature.weather.forecast.ThreeDayForecastAdapterDelegate
import com.hannesdorfmann.adapterdelegates4.ListDelegationAdapter

class CityDataAdapter(currentWeatherDelegate: CurrentWeatherAdapterDelegate,
                      threeDayForecastDelegate: ThreeDayForecastAdapterDelegate,
                      newsDelegate: NewsHeadlineAdapterDelegate,
                      titleAdapterDelegate: TitleAdapterDelegate,
                      failureItemAdapterDelegate: FailureItemAdapterDelegate) : ListDelegationAdapter<List<Any>>(), AutoUpdatableAdapter {
	init {
		delegatesManager.addDelegate(currentWeatherDelegate)
		delegatesManager.addDelegate(threeDayForecastDelegate)
		delegatesManager.addDelegate(newsDelegate)
		delegatesManager.addDelegate(titleAdapterDelegate)
		delegatesManager.addDelegate(failureItemAdapterDelegate)
	}

	override fun setItems(items: List<Any>?) {
		val previousItems = this.items.orEmpty()
		super.setItems(items)
		items?.let { autoNotify(previousItems, items) { o, n -> o == n } }
	}
}