package com.blahovici.accuweather_test.feature.weather.units

data class DailyTemperatureKelvin(val current: Double, val minMax: MinMaxKelvin)