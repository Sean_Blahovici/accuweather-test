package com.blahovici.accuweather_test.feature.weather.current

import com.blahovici.accuweather_test.feature.weather.WeatherApi.Companion.WEATHER_ICON_URL
import com.blahovici.accuweather_test.feature.weather.units.DailyTemperatureKelvin
import com.blahovici.accuweather_test.feature.weather.units.MinMaxKelvin

interface CurrentWeatherResponseMapper {
    fun map(apiResponse: OpenWeatherCurrentResponse) : CurrentWeather

    class Service : CurrentWeatherResponseMapper {
        override fun map(apiResponse: OpenWeatherCurrentResponse): CurrentWeather =
            CurrentWeather(apiResponse.name!!, buildTemperature(apiResponse), buildIconURL(apiResponse))

        private fun buildTemperature(response: OpenWeatherCurrentResponse): DailyTemperatureKelvin =
            DailyTemperatureKelvin(response.main!!.temp!!,
                MinMaxKelvin(response.main.temp_min!!, response.main.temp_max!!))

        private fun buildIconURL(response: OpenWeatherCurrentResponse): String {
            val icon = response.weather!![0]!!.icon
            return if (!icon.isNullOrEmpty()) String.format(WEATHER_ICON_URL, icon) else ""
        }
    }
}