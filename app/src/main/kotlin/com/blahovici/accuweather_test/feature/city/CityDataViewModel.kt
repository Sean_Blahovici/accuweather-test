package com.blahovici.accuweather_test.feature.city

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.blahovici.accuweather_test.core.failure.Failure

class CityDataViewModel(private val fetchCityData: FetchCityData,
                        private val buildCityDataViews: BuildCityDataViews) : ViewModel() {
    private var _cityDataViews = MutableLiveData<List<Any>>()
    var cityDataViews: LiveData<List<Any>> = _cityDataViews

    private var _failure = MutableLiveData<Failure>()
    var failure: LiveData<Failure> = _failure

    fun loadCityData(cityName: String) =
        fetchCityData.execute({
            it.fold(::handleFailure, ::buildCityDataViews)
        }, cityName, viewModelScope)

	internal fun buildCityDataViews(cityData: CityData) =
			buildCityDataViews.execute({
				it.fold(::handleFailure) { builtViews ->
					_cityDataViews.value = builtViews
				}
			}, cityData, viewModelScope)

    private fun handleFailure(failure: Failure) {
        _failure.value = failure
    }
}