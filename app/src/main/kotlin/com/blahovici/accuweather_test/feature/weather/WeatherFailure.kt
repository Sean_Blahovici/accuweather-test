package com.blahovici.accuweather_test.feature.weather

import com.blahovici.accuweather_test.core.failure.Failure

class WeatherFailure {
    class FetchCurrentWeatherError(apiResponse: String?) : Failure.ThirdPartyFailure(apiResponse)
    class FetchForecastError(apiResponse: String?) : Failure.ThirdPartyFailure(apiResponse)
}