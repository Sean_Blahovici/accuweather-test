package com.blahovici.accuweather_test.feature.city

import arrow.core.Either
import com.blahovici.accuweather_test.core.failure.Failure
import com.blahovici.accuweather_test.feature.news.TopNewsHeadlines
import com.blahovici.accuweather_test.feature.weather.current.CurrentWeather
import com.blahovici.accuweather_test.feature.weather.forecast.ThreeDayForecast

data class CityData(val currentWeather: Either<Failure, CurrentWeather>,
                    val threeDayForecast: Either<Failure, ThreeDayForecast>,
                    val topHeadlines: Either<Failure, TopNewsHeadlines>)