package com.blahovici.accuweather_test.feature.news

import arrow.core.Either
import com.blahovici.accuweather_test.core.failure.Failure
import com.blahovici.accuweather_test.core.fn.tryOrNull

interface NewsRepository {
    fun fetchTopNewsHeadlines(cityName: String) : Either<Failure, TopNewsHeadlines>

    class Service(private val newsApi: NewsApi,
                  private val newsResponseMapper: NewsResponseMapper) : NewsRepository {
        override fun fetchTopNewsHeadlines(cityName: String): Either<Failure, TopNewsHeadlines> {
            tryOrNull { newsApi.topHeadlines(cityName).execute() }?.let { apiResponse ->
                apiResponse.body()?.let { fetchedNews ->
                    return Either.Right(newsResponseMapper.map(fetchedNews))
                }
                return Either.Left(NewsFailure.FetchTopHeadlinesFailure(apiResponse.toString()))
            } ?: return Either.Left(Failure.NetworkConnection)
        }
    }
}