package com.blahovici.accuweather_test.feature.news

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsApi {
    companion object {
        private const val PARAM_QUERY = "q"
        private const val PARAM_API_KEY = "apiKey"
        private const val TOP_HEADLINES = "top-headlines?"

        private const val API_KEY = "af4dd8433ecd4b8bad04138007900385"

        const val NEWS_BASE_URL = "https://newsapi.org/v2/"
    }

    @GET(TOP_HEADLINES) fun topHeadlines(@Query(PARAM_QUERY) cityNameQuery: String,
                                         @Query(PARAM_API_KEY) apiKey: String = API_KEY): Call<TopNewsHeadlinesApiResponse>
}