package com.blahovici.accuweather_test.feature.weather.current

import androidx.recyclerview.widget.RecyclerView
import com.blahovici.accuweather_test.R
import com.blahovici.accuweather_test.core.adapter.AppAdapterDelegate
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.row_current_weather.view.*

class CurrentWeatherAdapterDelegate : AppAdapterDelegate<List<Any>>() {
	override fun layoutId(): Int = R.layout.row_current_weather

	override fun isForViewType(items: List<Any>, position: Int): Boolean =
			items[position] is CurrentWeatherView

	override fun onBindViewHolder(items: List<Any>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
		val view = items[position] as CurrentWeatherView
		holder.itemView.apply {
			rowCurrentWeatherTitle.text = view.cityName
			rowCurrentWeatherMinMax.text = view.minMaxLabel
			rowCurrentWeatherCurrent.text = view.currentLabel
			Glide.with(activityContext.applicationContext).load(view.iconUrl).into(rowCurrentWeatherIcon)
		}
	}
}