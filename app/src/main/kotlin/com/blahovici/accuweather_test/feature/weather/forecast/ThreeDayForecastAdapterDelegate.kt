package com.blahovici.accuweather_test.feature.weather.forecast

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.blahovici.accuweather_test.R
import com.blahovici.accuweather_test.core.adapter.AppAdapterDelegate
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.layout_icon_with_text.view.*
import kotlinx.android.synthetic.main.row_three_day_forecast.view.*

class ThreeDayForecastAdapterDelegate : AppAdapterDelegate<List<Any>>() {
	override fun layoutId(): Int = R.layout.row_three_day_forecast

	override fun isForViewType(items: List<Any>, position: Int): Boolean = items[position] is ThreeDayForecastView

	override fun onBindViewHolder(items: List<Any>, position: Int, holder: RecyclerView.ViewHolder, payloads: MutableList<Any>) {
		val view = items[position] as ThreeDayForecastView
		holder.itemView.apply {
			populateDay1Layout(view)
			populateDay2Layout(view)
			populateDay3Layout(view)
		}
	}

	private fun View.populateDay1Layout(view: ThreeDayForecastView) {
		rowThreeDayForecastDay1.layoutIconWithTextText.text = view.day1.label
		Glide.with(activityContext.applicationContext).load(view.day1.iconUrl)
				.into(rowThreeDayForecastDay1.layoutIconWithTextIcon)
	}

	private fun View.populateDay2Layout(view: ThreeDayForecastView) {
		rowThreeDayForecastDay2.layoutIconWithTextText.text = view.day2.label
		Glide.with(activityContext.applicationContext).load(view.day2.iconUrl)
				.into(rowThreeDayForecastDay2.layoutIconWithTextIcon)
	}

	private fun View.populateDay3Layout(view: ThreeDayForecastView) {
		rowThreeDayForecastDay3.layoutIconWithTextText.text = view.day3.label
		Glide.with(activityContext.applicationContext).load(view.day3.iconUrl)
				.into(rowThreeDayForecastDay3.layoutIconWithTextIcon)
	}
}