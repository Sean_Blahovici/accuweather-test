package com.blahovici.accuweather_test.feature.weather.forecast

import com.blahovici.accuweather_test.feature.weather.WeatherApi.Companion.WEATHER_ICON_URL
import com.blahovici.accuweather_test.feature.weather.units.MinMaxKelvin

interface ForecastResponseMapper {
    fun map(apiResponse: OpenWeatherForecastResponse): ThreeDayForecast

    class Service : ForecastResponseMapper {
        override fun map(apiResponse: OpenWeatherForecastResponse): ThreeDayForecast {
            val orderedBlocks = apiResponse.list?.sortedBy { it.dt }
            return ThreeDayForecast(
                BuildDayForecast().invoke(orderedBlocks?.subList(0, 7)),
                BuildDayForecast().invoke(orderedBlocks?.subList(8, 15)),
                BuildDayForecast().invoke(orderedBlocks?.subList(16, 23))
            )
        }

        class BuildDayForecast {
            private var temperatures = arrayListOf<Double>()
            private val listOfIconIds = arrayListOf<String?>()

            fun invoke(dailyBlocks: List<OpenWeatherForecastResponse.ForecastBlock>?): ThreeDayForecast.Forecast {
                dailyBlocks?.forEach {
                    temperatures.add(it.main?.temp!!)
                    listOfIconIds.add(it.weather?.first()?.icon)
                }
                val mostCommonIconId = listOfIconIds.groupingBy { it }.eachCount().maxBy { it.value }?.key!!
                return ThreeDayForecast.Forecast(
                    MinMaxKelvin(temperatures.min()!!, temperatures.max()!!),
                    String.format(WEATHER_ICON_URL, mostCommonIconId))
            }
        }
    }
}