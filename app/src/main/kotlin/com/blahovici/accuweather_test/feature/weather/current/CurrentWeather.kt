package com.blahovici.accuweather_test.feature.weather.current

import com.blahovici.accuweather_test.feature.weather.units.DailyTemperatureKelvin

data class CurrentWeather(val cityName: String, val dailyTemperature: DailyTemperatureKelvin, val iconUrl: String)