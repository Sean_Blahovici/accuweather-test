package com.blahovici.accuweather_test.feature.news

data class TopNewsHeadlines(val headlines: List<Headline>) {
    data class Headline(val title: String, val source: String, val imageUrl: String)
}