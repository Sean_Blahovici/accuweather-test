package com.blahovici.accuweather_test.feature.city

import arrow.core.Either
import com.blahovici.accuweather_test.core.failure.Failure
import com.blahovici.accuweather_test.core.interactor.UseCase
import com.blahovici.accuweather_test.feature.news.NewsRepository
import com.blahovici.accuweather_test.feature.news.TopNewsHeadlines
import com.blahovici.accuweather_test.feature.weather.WeatherRepository
import com.blahovici.accuweather_test.feature.weather.current.CurrentWeather
import com.blahovici.accuweather_test.feature.weather.forecast.ThreeDayForecast
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope

class FetchCityData(private val weatherRepository: WeatherRepository,
                    private val newsRepository: NewsRepository) : UseCase<CityData, String>() {
	override suspend fun run(params: String): Either<Failure, CityData> =
			coroutineScope {
				val fetchCurrentWeatherJob = async { weatherRepository.fetchCurrent(params) }
				val fetchForecastJob = async { weatherRepository.fetchForecast(params) }
				val fetchNewsJob = async { newsRepository.fetchTopNewsHeadlines(params) }

				val (currentWeather, threeDayForecast, topHeadlines) = awaitAllJobs(fetchCurrentWeatherJob, fetchForecastJob, fetchNewsJob)

				if (allJobsHaveFailed(currentWeather, threeDayForecast, topHeadlines))
					Either.Left(CityDataFailure.FailedToFetchAnyData())
				else
					Either.Right(CityData(currentWeather, threeDayForecast, topHeadlines))
			}

	private suspend fun awaitAllJobs(fetchCurrentWeatherJob: Deferred<Either<Failure, CurrentWeather>>, fetchForecastJob: Deferred<Either<Failure, ThreeDayForecast>>, fetchNewsJob: Deferred<Either<Failure, TopNewsHeadlines>>): Triple<Either<Failure, CurrentWeather>, Either<Failure, ThreeDayForecast>, Either<Failure, TopNewsHeadlines>> {
		val currentWeather = fetchCurrentWeatherJob.await()
		val threeDayForecast = fetchForecastJob.await()
		val topHeadlines = fetchNewsJob.await()
		return Triple(currentWeather, threeDayForecast, topHeadlines)
	}

	private fun allJobsHaveFailed(currentWeather: Either<Failure, CurrentWeather>,
	                              threeDayForecast: Either<Failure, ThreeDayForecast>,
	                              topHeadlines: Either<Failure, TopNewsHeadlines>): Boolean =
			currentWeather.isLeft() && threeDayForecast.isLeft() && topHeadlines.isLeft()
}