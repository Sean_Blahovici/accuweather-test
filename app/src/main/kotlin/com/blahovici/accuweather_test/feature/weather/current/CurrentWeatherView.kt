package com.blahovici.accuweather_test.feature.weather.current

data class CurrentWeatherView(val cityName: String,
                              val minMaxLabel: String,
                              val currentLabel: String,
                              val iconUrl: String)