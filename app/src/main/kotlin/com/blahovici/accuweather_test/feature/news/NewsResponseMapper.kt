package com.blahovici.accuweather_test.feature.news

interface NewsResponseMapper {
    fun map(apiResponse: TopNewsHeadlinesApiResponse): TopNewsHeadlines

    class Service : NewsResponseMapper {
        override fun map(apiResponse: TopNewsHeadlinesApiResponse): TopNewsHeadlines =
            TopNewsHeadlines(arrayListOf<TopNewsHeadlines.Headline>().apply {
                apiResponse.articles?.forEach {
                    add(TopNewsHeadlines.Headline(it?.title!!, it.source?.name!!, it.urlToImage!!))
                }
            })
    }
}