package com.blahovici.accuweather_test.feature.weather.units

data class MinMaxKelvin(val min: Double, val max: Double)