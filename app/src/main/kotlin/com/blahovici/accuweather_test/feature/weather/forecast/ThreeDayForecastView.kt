package com.blahovici.accuweather_test.feature.weather.forecast

data class ThreeDayForecastView(val day1: ForecastView,
                                val day2: ForecastView,
                                val day3: ForecastView) {
	data class ForecastView(val label: String, val iconUrl: String)
}