package com.blahovici.accuweather_test.feature.weather.forecast

import com.blahovici.accuweather_test.feature.weather.units.MinMaxKelvin

data class ThreeDayForecast(val day1: Forecast,
                            val day2: Forecast,
                            val day3: Forecast) {
    data class Forecast(val minMaxKelvin: MinMaxKelvin, val iconUrl: String)
}