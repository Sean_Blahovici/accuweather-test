package com.blahovici.accuweather_test.feature.city

import arrow.core.Either
import com.blahovici.accuweather_test.core.failure.Failure
import com.blahovici.accuweather_test.core.interactor.UseCase
import com.blahovici.accuweather_test.core.ui.FailureItemView
import com.blahovici.accuweather_test.core.ui.TitleView
import com.blahovici.accuweather_test.feature.news.NewsFailure
import com.blahovici.accuweather_test.feature.news.NewsHeadlineView
import com.blahovici.accuweather_test.feature.news.TopNewsHeadlines
import com.blahovici.accuweather_test.feature.weather.WeatherFailure
import com.blahovici.accuweather_test.feature.weather.current.CurrentWeather
import com.blahovici.accuweather_test.feature.weather.current.CurrentWeatherView
import com.blahovici.accuweather_test.feature.weather.forecast.ThreeDayForecast
import com.blahovici.accuweather_test.feature.weather.forecast.ThreeDayForecastView
import com.blahovici.accuweather_test.feature.weather.units.DailyTemperatureKelvin
import com.blahovici.accuweather_test.feature.weather.units.MinMaxKelvin
import kotlin.math.roundToInt

class BuildCityDataViews : UseCase<List<Any>, CityData>() {
	override suspend fun run(params: CityData): Either<Failure, List<Any>> = Either.Right(
			arrayListOf<Any>().apply {
				buildCurrentWeatherView(params.currentWeather)
				buildForecastView(params.threeDayForecast)
				add(TitleView("News Highlights"))
				buildHeadlineViews(params.topHeadlines)
			})

	private fun ArrayList<Any>.buildCurrentWeatherView(currentWeatherResult: Either<Failure, CurrentWeather>) {
		currentWeatherResult.fold({
			val failure = it as WeatherFailure.FetchCurrentWeatherError
			add(FailureItemView("Current Weather", failure.apiResponse as String))
		}) {
			add(CurrentWeatherView(it.cityName, buildCurrentWeatherMinMaxLabel(it.dailyTemperature),
					buildCurrentWeatherLabel(it.dailyTemperature), it.iconUrl))
		}
	}

	private fun ArrayList<Any>.buildForecastView(forecastResult: Either<Failure, ThreeDayForecast>) {
		forecastResult.fold({
			val failure = it as WeatherFailure.FetchForecastError
			add(FailureItemView("Next 3 day forecast", failure.apiResponse as String))
		}) {
			add(ThreeDayForecastView(buildForecastView(it.day1), buildForecastView(it.day2), buildForecastView(it.day3)))
		}
	}

	private fun ArrayList<Any>.buildHeadlineViews(topHeadlineResults: Either<Failure, TopNewsHeadlines>) {
		topHeadlineResults.fold({
			val failure = it as NewsFailure.FetchTopHeadlinesFailure
			add(FailureItemView("News headlines", failure.apiResponse as String))
		}) {
			it.headlines.forEach { headline ->
				add(buildHeadlineView(headline))
			}
		}
	}

	private fun buildCurrentWeatherMinMaxLabel(dailyTemperature: DailyTemperatureKelvin): String =
			"max ${kelvinToCelsius(dailyTemperature.minMax.max)} / min ${kelvinToCelsius(dailyTemperature.minMax.min)}"

	private fun buildCurrentWeatherLabel(dailyTemperature: DailyTemperatureKelvin): String =
			"Current ${kelvinToCelsius(dailyTemperature.current)}"

	private fun buildForecastView(forecast: ThreeDayForecast.Forecast) =
			ThreeDayForecastView.ForecastView(buildForecastMaxMinLabel(forecast.minMaxKelvin), forecast.iconUrl)

	private fun buildForecastMaxMinLabel(minMaxKelvin: MinMaxKelvin): String =
			"${kelvinToCelsius(minMaxKelvin.max)}/${kelvinToCelsius(minMaxKelvin.min)}"

	private fun kelvinToCelsius(kelvin: Double) = (kelvin - 273.15).roundToInt()

	private fun buildHeadlineView(headline: TopNewsHeadlines.Headline): NewsHeadlineView =
			NewsHeadlineView(headline.title, "By ${headline.source}", headline.imageUrl)

}