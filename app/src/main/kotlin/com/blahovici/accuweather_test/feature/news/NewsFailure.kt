package com.blahovici.accuweather_test.feature.news

import com.blahovici.accuweather_test.core.failure.Failure

class NewsFailure {
    class FetchTopHeadlinesFailure(apiResponse: String?) : Failure.ThirdPartyFailure(apiResponse)
}