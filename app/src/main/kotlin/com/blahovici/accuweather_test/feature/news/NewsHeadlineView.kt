package com.blahovici.accuweather_test.feature.news

data class NewsHeadlineView(val title: String,
                            val bySourceLabel: String,
                            val imageUrl: String)