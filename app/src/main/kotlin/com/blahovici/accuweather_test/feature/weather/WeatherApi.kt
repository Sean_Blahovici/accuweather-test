package com.blahovici.accuweather_test.feature.weather

import com.blahovici.accuweather_test.feature.weather.current.OpenWeatherCurrentResponse
import com.blahovici.accuweather_test.feature.weather.forecast.OpenWeatherForecastResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {
	companion object {
		private const val PARAM_QUERY = "q"
		private const val PARAM_APP_ID = "appid"
		private const val WEATHER = "weather?"
		private const val FORECAST = "forecast?"

		private const val API_KEY = "462791b808e7130777964e578cb3e83c"

		const val WEATHER_ICON_URL = "https://openweathermap.org/img/w/%s.png"
		const val OPEN_WEATHER_BASE_URL = "https://api.openweathermap.org/data/2.5/"
	}

	@GET(WEATHER) fun currentWeather(@Query(PARAM_QUERY) cityNameQuery: String,
	                                 @Query(PARAM_APP_ID) apiKey: String = API_KEY): Call<OpenWeatherCurrentResponse>

	@GET(FORECAST) fun weatherForecast(@Query(PARAM_QUERY) cityNameQuery: String,
	                                   @Query(PARAM_APP_ID) apiKey: String = API_KEY): Call<OpenWeatherForecastResponse>
}