package com.blahovici.accuweather_test.feature.city

import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.blahovici.accuweather_test.R
import com.blahovici.accuweather_test.core.MainActivity
import kotlinx.android.synthetic.main.fragment_city_data.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel

class CityDataFragment : Fragment() {
	private fun mainActivity(): MainActivity? = activity as? MainActivity
	private val cityDataAdapter: CityDataAdapter by inject()
	private val cityDataViewModel: CityDataViewModel by viewModel()

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		cityDataViewModel.cityDataViews.observe(this, Observer { renderViewsWhenReady(it) })
		cityDataViewModel.failure.observe(this, Observer { handleFailure() })
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_city_data, container, false)

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		setupViews()
	}

	private fun setupViews() {
		setupCitySearchBar()
		setupRV()
	}

	private fun setupCitySearchBar() {
		citySearchBar.setOnEditorActionListener { view, actionId, event ->
			if (userPressedDone(actionId, event))
				onUserEnteredCitySearchQuery(view)
			else
				false
		}
	}

	private fun onUserEnteredCitySearchQuery(view: TextView): Boolean {
		mainActivity()?.showProgress()
		cityDataViewModel.loadCityData(view.text.toString())
		mainActivity()?.hideKeyboard(citySearchBar)
		citySearchBar.clearFocus()
		return true
	}

	private fun userPressedDone(actionId: Int, event: KeyEvent?): Boolean =
			actionId == EditorInfo.IME_ACTION_SEARCH ||
					actionId == EditorInfo.IME_ACTION_NEXT ||
					actionId == EditorInfo.IME_ACTION_DONE ||
					event?.action == KeyEvent.ACTION_DOWN && event.keyCode == KeyEvent.KEYCODE_ENTER

	private fun setupRV() {
		cityDataRV.layoutManager = LinearLayoutManager(activity)
		cityDataRV.adapter = cityDataAdapter
	}

	private fun renderViewsWhenReady(views: List<Any>) = cityDataRV?.post {
		renderViews(views)
	}

	private fun renderViews(views: List<Any>) {
		showList()
		cityDataAdapter.items = views
		mainActivity()?.hideProgress()
	}

	private fun showList() {
		citySearchFailureTextView.visibility = View.INVISIBLE
		cityDataRV.visibility = View.VISIBLE
	}

	private fun handleFailure() {
		citySearchFailureTextView.visibility = View.VISIBLE
		cityDataRV.visibility = View.INVISIBLE
	}
}