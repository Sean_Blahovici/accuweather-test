package com.blahovici.accuweather_test.feature.city

import com.blahovici.accuweather_test.core.failure.Failure

class CityDataFailure {
	class FailedToFetchAnyData : Failure.FeatureFailure()
}