package com.blahovici.accuweather_test.feature.weather.forecast

data class OpenWeatherForecastResponse(
    val city: City? = null,
    val cnt: Int? = null,
    val cod: String? = null,
    val list: List<ForecastBlock>? = null,
    val message: Int? = null
) {
    data class City(
        val coord: Coord? = null,
        val country: String? = null,
        val id: Int? = null,
        val name: String? = null,
        val population: Int? = null,
        val sunrise: Int? = null,
        val sunset: Int? = null,
        val timezone: Int? = null
    ) {
        data class Coord(
            val lat: Double? = null,
            val lon: Double? = null
        )
    }

    data class ForecastBlock(
        val clouds: Clouds? = null,
        val dt: Int? = null,
        val dt_txt: String? = null,
        val main: Main? = null,
        val pop: Double? = null,
        val rain: Rain? = null,
        val sys: Sys? = null,
        val visibility: Int? = null,
        val weather: List<Weather?>? = null,
        val wind: Wind? = null
    ) {
        data class Clouds(
            val all: Int? = null
        )

        data class Main(
            val feels_like: Double? = null,
            val grnd_level: Int? = null,
            val humidity: Int? = null,
            val pressure: Int? = null,
            val sea_level: Int? = null,
            val temp: Double? = null,
            val temp_kf: Double? = null,
            val temp_max: Double? = null,
            val temp_min: Double? = null
        )

        data class Rain(
            val `3h`: Double? = null
        )

        data class Sys(
            val pod: String? = null
        )

        data class Weather(
            val description: String? = null,
            val icon: String? = null,
            val id: Int? = null,
            val main: String? = null
        )

        data class Wind(
            val deg: Int? = null,
            val speed: Double? = null
        )
    }
}