package com.blahovici.accuweather_test.core

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.coroutines.CoroutineContext

@RunWith(MockitoJUnitRunner.Silent::class)
abstract class UnitTest : CoroutineScope {
    @Suppress("LeakingThis")
    @Rule
    @JvmField val injectMocks = InjectMocksRule.create(this@UnitTest)

    override val coroutineContext: CoroutineContext =
        Dispatchers.Main + SupervisorJob()

    @Before
    fun setupCoroutines() {
        Dispatchers.setMain(testDispatcher)
    }

    @After
    fun tearDown() {
        coroutineContext[Job]?.cancel()
        Dispatchers.resetMain()
    }

    @get:Rule
    val rule = InstantTaskExecutorRule()
    protected val testDispatcher = TestCoroutineDispatcher()
}