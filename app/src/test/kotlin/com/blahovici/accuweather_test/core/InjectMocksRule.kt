/*
 * Copyright (c) 2020, Sean Blahovici - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sean Blahovici <sean.blahovici@gmail.com>, 15/04/20 10:04 AM
 */

package com.blahovici.accuweather_test.core

import org.junit.rules.TestRule
import org.mockito.MockitoAnnotations

class InjectMocksRule {
    companion object {
        fun create(testClass: Any) = TestRule { statement, _ ->
            MockitoAnnotations.initMocks(testClass)
            statement
        }
    }
}
