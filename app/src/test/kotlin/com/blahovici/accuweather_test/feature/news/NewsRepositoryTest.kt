package com.blahovici.accuweather_test.feature.news

import com.blahovici.accuweather_test.core.UnitTest
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import junit.framework.TestCase
import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Test
import retrofit2.Call
import retrofit2.Response
import test_common.NewsTestData.sampleTopNewsHeadlines
import test_common.NewsTestData.sampleTopNewsHeadlinesApiResponse
import test_common.TestConstants.CITY_NAME
import test_common.TestConstants.INVALID_CITY_API_RESPONSE
import test_common.TestConstants.INVALID_CITY_NAME

class NewsRepositoryTest : UnitTest() {
	private val mockNewsApi: NewsApi = mock()
	private val mockNewsResponseMapper: NewsResponseMapper = mock()
    private val newsRepository = NewsRepository.Service(mockNewsApi, mockNewsResponseMapper)

	private val mockNewsApiCall: Call<TopNewsHeadlinesApiResponse> = mock()
	private val mockNewsApiResponse: Response<TopNewsHeadlinesApiResponse> = mock()
	
	@Test fun givenApiReturningValidResponse_whenFetchingNews_thenReturnMappedNews() {
		givenApiReturnsNewsResponse(sampleTopNewsHeadlinesApiResponse)
		given(mockNewsResponseMapper.map(eq(sampleTopNewsHeadlinesApiResponse))).willReturn(sampleTopNewsHeadlines)

		val result = newsRepository.fetchTopNewsHeadlines(CITY_NAME)

		result.toOption().orNull() shouldBeEqualTo sampleTopNewsHeadlines
	}

	@Test fun givenApiReturningInvalidNewsResponse_whenFetchingNews_thenReturnFailure() {
		givenNewsApi_returnsInvalidCityResponse()

		val result = newsRepository.fetchTopNewsHeadlines(INVALID_CITY_NAME)

		result.fold({
			(it as NewsFailure.FetchTopHeadlinesFailure).apiResponse `should be equal to` INVALID_CITY_API_RESPONSE
		}) { TestCase.fail() }
	}

	private fun givenApiReturnsNewsResponse(newsResponse: TopNewsHeadlinesApiResponse) {
		given(mockNewsApiCall.execute()).willReturn(mockNewsApiResponse)
		given(mockNewsApiResponse.isSuccessful).willReturn(true)
		given(mockNewsApiResponse.body()).willReturn(newsResponse)
		given(mockNewsApi.topHeadlines(CITY_NAME)).willReturn(mockNewsApiCall)
	}

	private fun givenNewsApi_returnsInvalidCityResponse() {
		given(mockNewsApiCall.execute()).willReturn(mockNewsApiResponse)
		given(mockNewsApiResponse.isSuccessful).willReturn(false)
		given(mockNewsApiResponse.toString()).willReturn(INVALID_CITY_API_RESPONSE)
		given(mockNewsApi.topHeadlines(INVALID_CITY_NAME)).willReturn(mockNewsApiCall)
	}
}