package com.blahovici.accuweather_test.feature.city

import androidx.lifecycle.Observer
import arrow.core.Either
import com.blahovici.accuweather_test.core.UnitTest
import com.blahovici.accuweather_test.core.failure.Failure
import com.nhaarman.mockitokotlin2.*
import org.amshove.kluent.`should be equal to`
import org.junit.Test
import org.mockito.Mockito
import test_common.CityTestData.sampleCityData
import test_common.TestConstants.CITY_NAME

class CityDataViewModelTest : UnitTest() {
	private val mockFetchCityData: FetchCityData = mock()
	private val mockBuildCityDataViews: BuildCityDataViews = mock()
	private val cityDataViewModel = CityDataViewModel(mockFetchCityData, mockBuildCityDataViews)

	@Test fun givenFetchCityDataReturnsData_whenCallingLoadCityData_thenInvokeBuildCityDataUseCase() {
		givenFetchCityDataReturnsData(sampleCityData)

		cityDataViewModel.loadCityData(CITY_NAME)

		argumentCaptor<CityData>().apply {
			verify(mockBuildCityDataViews, times(1)).execute(any(), capture(), any())
			firstValue `should be equal to` sampleCityData
		}
	}

	@Test fun givenBuildCityDataViewsReturnsViews_whenCallingBuildCityDataViews_shouldInvokeViewsLiveData() {
		val views = listOf<Any>(mock())
		givenBuildCityDataViewsReturnsViews(views, sampleCityData)
		val observer: Observer<List<Any>> = mock()
		cityDataViewModel.cityDataViews.observeForever(observer)

		cityDataViewModel.buildCityDataViews(sampleCityData)

		argumentCaptor<List<Any>>().apply {
			verify(observer, times(1)).onChanged(capture())
			firstValue `should be equal to` views
		}
	}

	private fun givenFetchCityDataReturnsData(cityData: CityData) {
		Mockito.`when`(mockFetchCityData.execute(any(), eq(CITY_NAME), any())).thenAnswer { answer ->
			answer.getArgument<(Either<Failure, CityData>) -> Unit>(0)(Either.Right(cityData))
		}
	}

	private fun givenBuildCityDataViewsReturnsViews(views: List<Any>, cityData: CityData) {
		Mockito.`when`(mockBuildCityDataViews.execute(any(), eq(cityData), any())).thenAnswer { answer ->
			answer.getArgument<(Either<Failure, List<Any>>) -> Unit>(0)(Either.Right(views))
		}
	}
}