package com.blahovici.accuweather_test.feature.city

import com.blahovici.accuweather_test.core.UnitTest
import com.blahovici.accuweather_test.core.ui.TitleView
import com.blahovici.accuweather_test.feature.news.NewsHeadlineView
import com.blahovici.accuweather_test.feature.weather.current.CurrentWeatherView
import com.blahovici.accuweather_test.feature.weather.forecast.ThreeDayForecastView
import kotlinx.coroutines.test.runBlockingTest
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Test
import test_common.CityTestData.sampleCityData
import test_common.TestConstants.CITY_NAME

class BuildCityDataViewsTest : UnitTest() {
	private val buildCityDataViews = BuildCityDataViews()

	@Test fun givenSampleCityData_whenBuildingViews_shouldBuildCurrentWeatherViewCorrectly() = testDispatcher.runBlockingTest {
		val views = buildCityDataViews.run(sampleCityData)
		views.toOption().orNull()!!.apply {
			(this[0] as CurrentWeatherView).validate()
		}
	}

	@Test fun givenSampleCityData_whenBuildingViews_shouldBuildForecastViewCorrectly() = testDispatcher.runBlockingTest {
		val views = buildCityDataViews.run(sampleCityData)
		views.toOption().orNull()!!.apply {
			(this[1] as ThreeDayForecastView).validate()
		}
	}

	@Test fun givenSampleCityData_whenBuildingViews_shouldBuildNewsViewsCorrectly() = testDispatcher.runBlockingTest {
		val views = buildCityDataViews.run(sampleCityData)
		views.toOption().orNull()!!.apply {
			(this[2] as TitleView).title shouldBeEqualTo "News Highlights"
			(this[3] as NewsHeadlineView).validate()
		}
	}

	private fun CurrentWeatherView.validate() {
		cityName shouldBeEqualTo CITY_NAME
		minMaxLabel shouldBeEqualTo "max 22 / min 20"
		currentLabel shouldBeEqualTo "Current 21"
		iconUrl shouldBeEqualTo "https://openweathermap.org/img/w/04d.png"
	}

	private fun ThreeDayForecastView.validate() {
		day1.label shouldBeEqualTo  "24/17"
		day1.iconUrl shouldBeEqualTo  "https://openweathermap.org/img/w/04d.png"
	}

	private fun NewsHeadlineView.validate() {
		title shouldBeEqualTo "Montreal Cognitive Assessment: Take the test that Donald Trump ‘aced’"
		bySourceLabel shouldBeEqualTo "By News.com.au"
		imageUrl shouldBeEqualTo "https://cdn.newsapi.com.au/image/v1/f1683f224d9c239961d0ed169c4a3e93?width=650"
	}
}