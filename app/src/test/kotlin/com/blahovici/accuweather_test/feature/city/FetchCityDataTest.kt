package com.blahovici.accuweather_test.feature.city

import arrow.core.Either
import com.blahovici.accuweather_test.core.UnitTest
import com.blahovici.accuweather_test.core.failure.Failure
import com.blahovici.accuweather_test.feature.news.NewsFailure
import com.blahovici.accuweather_test.feature.news.NewsRepository
import com.blahovici.accuweather_test.feature.weather.WeatherFailure
import com.blahovici.accuweather_test.feature.weather.WeatherRepository
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import kotlinx.coroutines.test.runBlockingTest
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldBeInstanceOf
import org.junit.Assert.fail
import org.junit.Test
import test_common.TestConstants.CITY_NAME
import test_common.WeatherTestData.sampleCurrentWeather
import test_common.WeatherTestData.sampleThreeDayForecast

class FetchCityDataTest : UnitTest() {
    private val mockWeatherRepository: WeatherRepository = mock()
    private val mockNewsRepository: NewsRepository = mock()
    private val fetchCityData = FetchCityData(mockWeatherRepository, mockNewsRepository)

    @Test fun givenAllRepositoriesFetchData_whenRunningUseCase_shouldAssembleCityDataCorrectly()
            = testDispatcher.runBlockingTest {
        givenWeatherRepositoryReturningValidValues_butNewsReturningFailure()

        val result = fetchCityData.run(CITY_NAME)

        assertCityDataCorrectlyAssembled(result)
    }

	@Test fun givenAllRepositoriesFailToFetchData_whenRunningUseCase_shouldReturnFailure()
			= testDispatcher.runBlockingTest {
		givenWeatherRepositoryReturningAllFailures()

		val result = fetchCityData.run(CITY_NAME)

		result.fold({ it shouldBeInstanceOf CityDataFailure.FailedToFetchAnyData::class}) { fail() }
	}

    private fun givenWeatherRepositoryReturningValidValues_butNewsReturningFailure() {
        given(mockWeatherRepository.fetchCurrent(CITY_NAME)).willReturn(Either.Right(sampleCurrentWeather))
        given(mockWeatherRepository.fetchForecast(CITY_NAME)).willReturn(Either.Right(sampleThreeDayForecast))
        given(mockNewsRepository.fetchTopNewsHeadlines(CITY_NAME))
            .willReturn(Either.Left(NewsFailure.FetchTopHeadlinesFailure("Server Error")))
    }

	private fun givenWeatherRepositoryReturningAllFailures() {
		given(mockWeatherRepository.fetchCurrent(CITY_NAME))
				.willReturn(Either.Left(WeatherFailure.FetchCurrentWeatherError("Server Error")))
		given(mockWeatherRepository.fetchForecast(CITY_NAME))
				.willReturn(Either.Left(WeatherFailure.FetchForecastError("Server Error")))
		given(mockNewsRepository.fetchTopNewsHeadlines(CITY_NAME))
				.willReturn(Either.Left(NewsFailure.FetchTopHeadlinesFailure("Server Error")))
	}

    private fun assertCityDataCorrectlyAssembled(result: Either<Failure, CityData>) {
        result.fold({ fail() }) {
            it.currentWeather.toOption().orNull() shouldBeEqualTo sampleCurrentWeather
            it.threeDayForecast.toOption().orNull()!!.day1 shouldBeEqualTo sampleThreeDayForecast.day1
            it.topHeadlines.fold({ failure ->
                (failure as NewsFailure.FetchTopHeadlinesFailure).apiResponse shouldBeEqualTo "Server Error"
            }) { fail() }
        }
    }
}