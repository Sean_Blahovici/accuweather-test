package com.blahovici.accuweather_test.feature.weather.forecast

import com.blahovici.accuweather_test.core.UnitTest
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Test
import test_common.WeatherTestData.sampleForecastApiResponse
import test_common.WeatherTestData.sampleThreeDayForecast

class ForecastResponseMapperTest : UnitTest() {
    private val mapper = ForecastResponseMapper.Service()

    @Test fun givenSampleApiResponse_whenMapped_thenReturnCorrectlyFormatted3DayForecast() {
		val threeDayForecast = mapper.map(sampleForecastApiResponse)
        threeDayForecast.day1 shouldBeEqualTo sampleThreeDayForecast.day1
	}
}