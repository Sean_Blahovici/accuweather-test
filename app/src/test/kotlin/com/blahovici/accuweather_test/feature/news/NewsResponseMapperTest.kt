package com.blahovici.accuweather_test.feature.news

import com.blahovici.accuweather_test.core.UnitTest
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Test
import test_common.NewsTestData.sampleTopNewsHeadlines
import test_common.NewsTestData.sampleTopNewsHeadlinesApiResponse

class NewsResponseMapperTest : UnitTest() {
    private val mapper = NewsResponseMapper.Service()

	@Test fun givenSampleApiResponse_whenMapped_thenReturnCorrectlyFormattedTopHeadlines() {
		val topHeadlines = mapper.map(sampleTopNewsHeadlinesApiResponse)
        topHeadlines.headlines.first() shouldBeEqualTo sampleTopNewsHeadlines.headlines.first()
	}
}