package com.blahovici.accuweather_test.feature.weather

import com.blahovici.accuweather_test.core.UnitTest
import com.blahovici.accuweather_test.feature.weather.current.CurrentWeatherResponseMapper
import com.blahovici.accuweather_test.feature.weather.current.OpenWeatherCurrentResponse
import com.blahovici.accuweather_test.feature.weather.forecast.ForecastResponseMapper
import com.blahovici.accuweather_test.feature.weather.forecast.OpenWeatherForecastResponse
import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.given
import com.nhaarman.mockitokotlin2.mock
import junit.framework.TestCase.fail
import org.amshove.kluent.`should be equal to`
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Test
import retrofit2.Call
import retrofit2.Response
import test_common.TestConstants.CITY_NAME
import test_common.TestConstants.INVALID_CITY_API_RESPONSE
import test_common.TestConstants.INVALID_CITY_NAME
import test_common.WeatherTestData.sampleCurrentWeather
import test_common.WeatherTestData.sampleCurrentWeatherApiResponse
import test_common.WeatherTestData.sampleForecastApiResponse
import test_common.WeatherTestData.sampleThreeDayForecast

class WeatherRepositoryTest : UnitTest() {
    private val mockWeatherApi : WeatherApi = mock()
    private val mockCurrentWeatherResponseMapper : CurrentWeatherResponseMapper = mock()
    private val mockForecastResponseMapper : ForecastResponseMapper = mock()
    private val weatherRepository = WeatherRepository.Service(mockWeatherApi,
        mockCurrentWeatherResponseMapper, mockForecastResponseMapper)

    private val mockCurrentWeatherApiCall: Call<OpenWeatherCurrentResponse> = mock()
    private val mockCurrentWeatherApiResponse: Response<OpenWeatherCurrentResponse> = mock()
    private val mockForecastApiCall: Call<OpenWeatherForecastResponse> = mock()
    private val mockForecastApiResponse: Response<OpenWeatherForecastResponse> = mock()

    @Test fun givenApiReturningValidResponse_whenFetchingCurrentWeather_thenReturnMappedCurrentWeather() {
        givenApiReturnsCurrentWeatherResponse(sampleCurrentWeatherApiResponse)
        given(mockCurrentWeatherResponseMapper.map(eq(sampleCurrentWeatherApiResponse))).willReturn(sampleCurrentWeather)

        val result = weatherRepository.fetchCurrent(CITY_NAME)

        result.toOption().orNull() shouldBeEqualTo sampleCurrentWeather
    }

    @Test fun givenApiReturningInvalidCityResponse_whenFetchingCurrentWeather_thenReturnFailure() {
        givenApiCurrentWeather_returnsInvalidCityResponse()

        val result = weatherRepository.fetchCurrent(INVALID_CITY_NAME)

        result.fold({
            (it as WeatherFailure.FetchCurrentWeatherError).apiResponse `should be equal to` INVALID_CITY_API_RESPONSE
        }) { fail() }
    }

    @Test fun givenApiReturningValidResponse_whenFetchingForecast_thenReturnMapped3DayForecast() {
        givenApiReturnsForecastResponse(sampleForecastApiResponse)
        given(mockForecastResponseMapper.map(eq(sampleForecastApiResponse))).willReturn(sampleThreeDayForecast)

        val result = weatherRepository.fetchForecast(CITY_NAME)

        result.toOption().orNull() shouldBeEqualTo sampleThreeDayForecast
    }

    @Test fun givenApiReturningInvalidCityResponse_whenFetchingForecast_thenReturnFailure() {
        givenApiForecast_returnsInvalidCityResponse()

        val result = weatherRepository.fetchForecast(INVALID_CITY_NAME)

        result.fold({
            (it as WeatherFailure.FetchForecastError).apiResponse `should be equal to` INVALID_CITY_API_RESPONSE
        }) { fail() }
    }

    private fun givenApiReturnsCurrentWeatherResponse(weatherResponse: OpenWeatherCurrentResponse) {
        given(mockCurrentWeatherApiCall.execute()).willReturn(mockCurrentWeatherApiResponse)
        given(mockCurrentWeatherApiResponse.isSuccessful).willReturn(true)
        given(mockCurrentWeatherApiResponse.body()).willReturn(weatherResponse)
        given(mockWeatherApi.currentWeather(CITY_NAME)).willReturn(mockCurrentWeatherApiCall)
    }

    private fun givenApiCurrentWeather_returnsInvalidCityResponse() {
        given(mockCurrentWeatherApiCall.execute()).willReturn(mockCurrentWeatherApiResponse)
        given(mockCurrentWeatherApiResponse.isSuccessful).willReturn(false)
        given(mockCurrentWeatherApiResponse.toString()).willReturn(INVALID_CITY_API_RESPONSE)
        given(mockWeatherApi.currentWeather(INVALID_CITY_NAME)).willReturn(mockCurrentWeatherApiCall)
    }

    private fun givenApiReturnsForecastResponse(forecastResponse: OpenWeatherForecastResponse) {
        given(mockForecastApiCall.execute()).willReturn(mockForecastApiResponse)
        given(mockForecastApiResponse.isSuccessful).willReturn(true)
        given(mockForecastApiResponse.body()).willReturn(forecastResponse)
        given(mockWeatherApi.weatherForecast(CITY_NAME)).willReturn(mockForecastApiCall)
    }

    private fun givenApiForecast_returnsInvalidCityResponse() {
        given(mockForecastApiCall.execute()).willReturn(mockForecastApiResponse)
        given(mockForecastApiResponse.isSuccessful).willReturn(false)
        given(mockForecastApiResponse.toString()).willReturn(INVALID_CITY_API_RESPONSE)
        given(mockWeatherApi.weatherForecast(INVALID_CITY_NAME)).willReturn(mockForecastApiCall)
    }
}