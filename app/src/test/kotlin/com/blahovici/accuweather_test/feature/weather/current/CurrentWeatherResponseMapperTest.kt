package com.blahovici.accuweather_test.feature.weather.current

import com.blahovici.accuweather_test.core.UnitTest
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Test
import test_common.WeatherTestData.sampleCurrentWeather
import test_common.WeatherTestData.sampleCurrentWeatherApiResponse

class CurrentWeatherResponseMapperTest : UnitTest() {
    private val mapper = CurrentWeatherResponseMapper.Service()

    @Test fun givenSampleApiResponse_whenMapped_thenReturnCorrectlyFormattedCurrentWeather() {
		val currentWeather = mapper.map(sampleCurrentWeatherApiResponse)
        currentWeather shouldBeEqualTo sampleCurrentWeather
	}
}