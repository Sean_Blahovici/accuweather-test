package test_common

object TestConstants {
    const val CITY_NAME = "Montreal"
    const val INVALID_CITY_NAME = "ADBHUDSBUYWHEB"
    const val INVALID_CITY_API_RESPONSE = "{\n" +
            "    \"cod\": \"404\",\n" +
            "    \"message\": \"city not found\"\n" +
            "}"
}