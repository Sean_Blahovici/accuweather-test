/*
 * Copyright (c) 2020, Sean Blahovici - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sean Blahovici <sean.blahovici@gmail.com>, 15/04/20 10:04 AM
 */

@file:Suppress("MemberVisibilityCanBePrivate")

package test_common

import com.blahovici.accuweather_test.feature.weather.WeatherApi.Companion.WEATHER_ICON_URL
import com.blahovici.accuweather_test.feature.weather.current.CurrentWeather
import com.blahovici.accuweather_test.feature.weather.current.OpenWeatherCurrentResponse
import com.blahovici.accuweather_test.feature.weather.forecast.OpenWeatherForecastResponse
import com.blahovici.accuweather_test.feature.weather.forecast.ThreeDayForecast
import com.blahovici.accuweather_test.feature.weather.units.DailyTemperatureKelvin
import com.blahovici.accuweather_test.feature.weather.units.MinMaxKelvin
import com.google.gson.GsonBuilder

object WeatherTestData {
	@Suppress("HasPlatformType")
	val sampleCurrentWeatherApiResponse by lazy {
		val rawJson = javaClass.getResourceAsStream("sample_current_weather_response.json")!!
				.bufferedReader().use { it.readText() }
		GsonBuilder().create().fromJson(rawJson, OpenWeatherCurrentResponse::class.java)
	}

	val sampleCurrentWeather by lazy {
		CurrentWeather("Montreal", DailyTemperatureKelvin(294.24, MinMaxKelvin(293.15, 295.37)),
				"https://openweathermap.org/img/w/04d.png")
	}

	@Suppress("HasPlatformType")
	val sampleForecastApiResponse by lazy {
		val rawJson = javaClass.getResourceAsStream("sample_forecast_response.json")!!
				.bufferedReader().use { it.readText() }
		GsonBuilder().create().fromJson(rawJson, OpenWeatherForecastResponse::class.java)
	}

	val sampleThreeDayForecast by lazy {
		ThreeDayForecast(buildDay1Forecast(), buildDay2Forecast(), buildDay3Forecast()) // Mocking to save time
	}

	private fun buildDay1Forecast(): ThreeDayForecast.Forecast =
			ThreeDayForecast.Forecast(MinMaxKelvin(290.17, 296.99),
					String.format(WEATHER_ICON_URL, "04d"))

	//The following forecasts are random numbers that don't match the json, to save time
	private fun buildDay2Forecast(): ThreeDayForecast.Forecast =
			ThreeDayForecast.Forecast(MinMaxKelvin(289.83, 302.81),
					String.format(WEATHER_ICON_URL, "10n"))

	private fun buildDay3Forecast(): ThreeDayForecast.Forecast =
			ThreeDayForecast.Forecast(MinMaxKelvin(284.17, 301.99),
					String.format(WEATHER_ICON_URL, "01d"))
}