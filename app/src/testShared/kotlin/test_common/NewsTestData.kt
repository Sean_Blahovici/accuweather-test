/*
 * Copyright (c) 2020, Sean Blahovici - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Sean Blahovici <sean.blahovici@gmail.com>, 15/04/20 10:04 AM
 */

@file:Suppress("MemberVisibilityCanBePrivate")

package test_common

import com.blahovici.accuweather_test.feature.news.TopNewsHeadlines
import com.blahovici.accuweather_test.feature.news.TopNewsHeadlinesApiResponse
import com.google.gson.GsonBuilder

object NewsTestData {
	@Suppress("HasPlatformType")
	val sampleTopNewsHeadlinesApiResponse by lazy {
		val rawJson = javaClass.getResourceAsStream("sample_top_news_headlines_response.json")!!
				.bufferedReader().use { it.readText() }
		GsonBuilder().create().fromJson(rawJson, TopNewsHeadlinesApiResponse::class.java)
	}

	val sampleTopNewsHeadlines by lazy {
		TopNewsHeadlines(listOf(TopNewsHeadlines
				.Headline(
						"Montreal Cognitive Assessment: Take the test that Donald Trump ‘aced’",
						"News.com.au", "https://cdn.newsapi.com.au/image/v1/f1683f224d9c239961d0ed169c4a3e93?width=650")))
	}
}