package test_common

import arrow.core.Either
import com.blahovici.accuweather_test.feature.city.CityData

object CityTestData {
	val sampleCityData : CityData by lazy {
		CityData(
				Either.Right(WeatherTestData.sampleCurrentWeather),
				Either.Right(WeatherTestData.sampleThreeDayForecast),
				Either.Right(NewsTestData.sampleTopNewsHeadlines)
		)
	}
}